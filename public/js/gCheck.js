﻿/* 
	by Hoyin
	Date: 2011-9-20

*/

function gCheckIsDate(sValue) {
    return /^\d{4}\-\d{2}\-\d{2}$/.test(sValue);
}
function gCheckIsName(sValue) {
    return /^[^~^`^!^@^#^$^%^^^&^*^(^)^\_^\-^+^=^{^}^\[^\]^\\^\|^:^;^\'^\"^<^,^\.^>^\/^\?^\d]+$/.test(sValue);
}
function gCheckIsEnglishName(sValue) {
    return /^[A-Z|a-z|\ |\-|\_|\.]+$/.test(sValue);
}

function gCheckIsValidEmailAddress(sValue) {
    return /^[_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,3}$/.test(sValue.toLowerCase());
}

function gCheckIsValidHongKongTelNo(sValue) {
    return /^[4|5|6|7|8|9]{1}[0-9]{7}$/.test(sValue);
}
function gCheckIsValidMacauTelNo(sValue) {
    return /^[\d]{8}$/.test(sValue);
}
function gCheckIsValidChinaTelNo(sValue) {
    return /^[\d]{8,13}$/.test(sValue);
}
function gCheckIsValidOtherTelNo(sValue) {
    return /^[\d]{7,20}$/.test(sValue);
} 
function gTrim(sValue) {
    return sValue.replace(/^\s+|\s+$/, '');
}
function gCheckIsEmptySingle(oControl) {

    if (oControl == null) {
        return true;
    }
    if (oControl.type == 'text') {
        oControl.value = gTrim(oControl.value);
        if (oControl.value != '') {
            return false;
        }
    }
    else if (oControl.type == 'select-one') {
        if (oControl.selectedIndex > 0) {
            return false;
        }
    }
    else if (oControl.type == 'radio') {
        if (oControl.checked) {
            return false;
        }
    }
    else if (oControl.type == 'checkbox') {
        if (oControl.checked) {
            return false;
        }
    }
    return true;
}

function gCheckIsRadioGroupEmpty(oControl) {
    var isEmpty = true
    if (oControl[0].type == 'radio') {
        for (i = 0; i < oControl.length; i++) {
            if (oControl[i].checked) {
                isEmpty = false
            }
        }
        return isEmpty
    }
    return true;
}


function gCheckIsEmptyMultiple(oAryControl) {
    if (oAryControl == null) {
        return true;
    }
    for ($i = 0; $i < oAryControl.length; $i++) {
        if (!gCheckIsEmptySingle(oAryControl[$i])) {

            return false;
        }
    }
    return true;
}

function gIsCheckedByName(name) {
    var checked = $("input[name*=" + name + "]:checked").length;
    if (checked == 0) {
        return false;
    }
    else {
        return true;
    }
}

function gIsValidTelNum(region_num, value) {

    var result = false;

    switch (region_num) {

        case "783": 
            result = gCheckIsValidHongKongTelNo(value); 
            break;

        case "784":  
            result = gCheckIsValidChinaTelNo(value); 
            break;

        case "785": 
            result = gCheckIsValidMacauTelNo(value); 
            break;

        case "786": 
            result = gCheckIsValidOtherTelNo(value); 
            break;

        default:

    } 
    return result; 
}